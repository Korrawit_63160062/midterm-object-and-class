package com.korrawit.car;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author DELL
 */
public class CarTemplate {

    public String carName;
    public String carType;
    public int numberOfWheel;
    public int numberOfDoor;
    public String engine;
    public int horsePower;
    public int topSpeed;

    public CarTemplate(String carName, String carType, int numberOfWheel, int numberOfDoor, String engine, int horsePower, int topSpeed) {

        this.carName = carName;
        this.carType = carType;
        this.numberOfWheel = numberOfWheel;
        this.numberOfDoor = numberOfDoor;
        this.engine = engine;
        this.horsePower = horsePower;
        this.topSpeed = topSpeed;
    }

    public void carDetail() {
        System.out.println(carName);
        System.out.println("Car type : " + carType);
        System.out.println("Number of wheels : " + numberOfWheel);
        System.out.println("Number of doors : " + numberOfDoor);
        System.out.println("Engine type : " + engine);
        System.out.println("Horse power : " + horsePower);
        System.out.println("Top speed (kph) : " + topSpeed);
    }
}
