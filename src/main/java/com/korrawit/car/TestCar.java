/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.korrawit.car;

/**
 *
 * @author DELL
 */
public class TestCar {

    public static void main(String[] args) {

        CarTemplate FerrariEnzo = new CarTemplate("Ferrari Enzo", "Coupe", 4, 2, "V12", 650, 355);
        FerrariEnzo.carDetail();
        
        CarTemplate LamborghiniCountach = new CarTemplate("Lamborghini Countach", "Sport", 4, 2, "V12", 770, 293);
        LamborghiniCountach.carDetail();
    }
}
